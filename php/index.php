<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Lab2</title>
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/default.css" rel="stylesheet" media="screen">
	</head>
    <body>                
        <div class="container">
        	<h1 class="title">Awesome recipe site</h1>        
				
				<form action="insert.php" method="post">
					
					<fieldset>
					
					<legend>Please create a recipe</legend>
					
						<label name="recipe">Recipe Name</label>
					<input class="input-xxlarge" type="text" name="title">
					
					<label>Ingredients</label>
					<div><input class="input-xxlarge" type="text" name="ing1"></div>
					<div><input class="input-xxlarge" type="text" name="ing2"></div>
					<div><input class="input-xxlarge" type="text" name="ing3"></div>
					<div><input class="input-xxlarge" type="text" name="ing4"></div>
					<div><input class="input-xxlarge" type="text" name="ing5"></div>
					
					<label>Directions</label>
					<div><textarea name="directions" class="input-block-level" rows="10"></textarea></div>
					
					<button type="submit" class="btn-btn-primary">Submit</button>
					
					</fieldset>
				</form>
		</div>
        
        
        <script src="http://code.jquery.com/jquery-latest.js"></script>
    	<script src="lib/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
