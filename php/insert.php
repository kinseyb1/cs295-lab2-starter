<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <title>CS295 Lab2</title>
        <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/default.css" rel="stylesheet" media="screen">
    </head>
    <body>                
        <div class="container">
        	<h1 class="title">Awesome recipe site</h1>        

		<label name="recipe">Recipe Name</label>
		<b><?php echo htmlentities($_POST["title"]); ?></b>
		<label>Ingredients</label>
		<ul>
			<li><?php echo htmlentities($_POST["ing1"]); ?> </li>
			<li><?php echo htmlentities($_POST["ing2"]); ?> </li>
			<li><?php echo htmlentities($_POST["ing3"]); ?> </li>
			<li><?php echo htmlentities($_POST["ing4"]); ?> </li>
			<li><?php echo htmlentities($_POST["ing5"]); ?> </li>
		</ul>
		<label>Directions</label>
		<?php echo htmlentities($_POST["directions"]); ?>
		</div>
        
        <script src="http://code.jquery.com/jquery-latest.js"></script>
    	<script src="lib/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
